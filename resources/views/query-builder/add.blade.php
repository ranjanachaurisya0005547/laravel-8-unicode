<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Add Data.....</title>
	<link rel="stylesheet" type="text/css" href="{{asset('/assets/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('/assets/bootstrap/css/bootstrap.css')}}">

	<script type="text/javascript" src="{{asset('/assets/js/jquery.js')}}"></script>
	<script type="text/javascript" src="{{asset('/assets/bootstrap/js/bootstrap.min.js')}}"></script>
</head>
<body>
<div class="container">
	<div class="card mt-5">
		<div class="card-header">
			Add New Data
		</div>
		<div class="card-body">
			<form action="" method="POST">
				<div class="from=group">
					<label class="form-label" form="name">Enter Your Name</label>
					<input type="text" id="name" name="name" class="form-control"/>
				</div>
				<div class="from=group">
					<label class="form-label" form="email">Enter Your Email</label>
					<input type="email" id="email" name="email" class="form-control"/>
				</div>
                <div class="from=group">
					<label class="form-label" form="mobile">Enter Your Mobile No.</label>
					<input type="text" id="mobile" name="mobile" class="form-control"/>
				</div>
				<div class="from=group">
					<label class="form-label" form="file">Choose Your Pick</label>
					<input type="file" id="file" name="pick" class="form-control"/>
				</div>


                <input type="submit" name="submit-btn" class="btn btn-primary mt-3" value="SUBMIT"/>
			</form>
		</div>
	</div>
</div>
</body>
</html>