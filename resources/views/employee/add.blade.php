
<!-----------------Menu Area-------------------->
@include('comman.header')

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6 emp_form">

			@if (session('status'))
<div class="alert alert-success" role="alert">
	<button type="button" class="close" data-dismiss="alert">×</button>
	{{ session('status') }}
</div>
@elseif(session('failed'))
<div class="alert alert-danger" role="alert">
	<button type="button" class="close" data-dismiss="alert">×</button>
	{{ session('failed') }}
</div>
@endif
			<form action="{{url('employee/registration/data')}}" method="POST">
				<p>Employee Registration....................</p>
				<input type="hidden" name="_token" value="{{csrf_token()}}"/>                          
				<div class="form-group">
				    <label for="name"><em class="start-color">* </em>Employee Name</label>
				    <input type="text" id="name" name="name" class="form-control"/>
				    @error('name')
                         <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror

			    </div>
			    <div class="form-group">
				    <label for="dob"><em class="start-color">* </em>DOB</label>
				    <input type="date" id="dob" name="dob" class="form-control"/>
				    @error('dob')
                         <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
			    </div>
			    <div class="form-group">
				    <label for="gender"><em class="start-color">* </em>Gender</label><br/>
				    <input type="radio" id="gender" name="gender" value="male" checked="true" />Male&nbsp;&nbsp;&nbsp;&nbsp;
				    <input type="radio" id="gender" name="gender" value="female">Female
				    @error('gender')
                         <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
			    </div>
                <div class="form-group">
				    <label for="address"><em class="start-color">* </em>Address</label>
				    <textarea id="address" name="address" class="form-control"></textarea>
				    @error('address')
                         <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
			    </div>
                <div class="form-group">
				    <label for="contact"><em class="start-color">* </em>Mobile Number</label>
				    <input type="text" id="contact" name="contact" class="form-control"/>
				    @error('contact')
                         <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
			    </div>
			    <div class="form-group">
				    <label for="email"><em class="start-color">* </em>Email</label>
				    <input type="text" id="email" name="email" class="form-control"/>
				    @error('email')
                         <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
			    </div>
			    <div class="form-group">
				    <label for="pincode"><em class="start-color">* </em>Pincode</label>
				    <input type="text" id="pincode" name="pincode" class="form-control"/>
				    @error('pincode')
                         <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
			    </div>
			    <div class="form-group">
				    <label for="doj"><em class="start-color">* </em>Joining Date</label>
				    <input type="date" id="doj" name="date_of_join" class="form-control"/>
				    @error('date_of_join')
                         <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
			    </div>
                 <input type="submit" name="employee_btn" value="SUBMIT" class="form-control bg-warning">




			</form>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>

<!-------------------------Footer Area---------->
@include('comman.footer')