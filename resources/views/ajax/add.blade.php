<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" name="csrf-token" content="{{ csrf_token() }}">
	<title>Ajax Registration..........</title>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/css/employee_css.css')}}"/>

      <script src="{{asset('assets/js/jquery.js')}}"></script>
      <script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
   
	<style>
		.error_msg{color: red;font-style: italic;}
		.success_msg{color: green;font-style: italic;}
	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-6 mb-3 mt-3">
			    <h2>User Registration.........</h2>
			    <div class="col-sm-3"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-6">
				<span class="error_msg"></span>
				<span class="success_msg"></span>
				<form id='dataform' class="mt-3">
					@csrf
				  <div class="form-group">
					   <label>Enter Name</label>
					   <input type="text" name="name" id="name" class="form-control"/>
				   </div>
				   <div class="form-group">
					   <label>Enter email</label>
					   <input type="email" name="email" id="email" class="form-control"/>
				   </div>
				   <div class="form-group">
					   <label>Enter Phone Number</label>
					   <input type="text" name="phone" id="phone" class="form-control"/>
				   </div>
				   <div class="form-group">
					   <label>Enter City Name</label>
					   <input type="text" name="city" id="city" class="form-control"/>
				   </div>
				   <div class="form-group">
					   <label>Enter Password</label>
					   <input type="password" name="password" id="password" class="form-control"/>
				   </div>
				   <input type="submit" name="sub_btn" id="sub_btn" value="SUBMIT" class="form-control bg-warning"/>
			</div>
		</form>
			<div class="col-sm-3"></div>
		</div>
	</div>


	<script type="text/javascript">
		$(document).ready(function(){
			$('#dataform').on('submit',function(event){
				event.preventDefault();
				//alert();
				$.ajax({
					url:"{{ url('/ajax_insertion')}}",
                    method:"POST",
                    data:new FormData(this),
                    dataType:'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    async:false,
                    success:function(data){
                        if(data.status==200){
               				     $('.success_msg').html(data.success_msg).css("display","block");
               				     $('.error_msg').css("display","none");
                        }else if(data.status==201){
                   	           $('.error_msg').html(data.error_msg).css("display","block");
                   	           $('.success_msg').css("display","none");
                        }
                    }
				})

			})
            // $("#sub_btn").click('on',function(){
            //    uname=$("#name").val();
            //    uemail=$("#email").val();
            //    uphone=$("#phone").val();
            //    ucity=$("#city").val();
            //    upassword=$("#password").val();

            //    // echo json_encode(uname);
            //    // console.log(uname+" "+uemail+" "+uphone+" "+ucity+" "+upassword);

            //    // // console.log($('#csrf_token').val());

            //    if(uname!="" && uemail!="" && uphone!="" && ucity!="" && upassword!=""){

            //    	$.ajax({
            //    		url:'http://localhost:7000/ajax_insertion',
            //    		type:"GET",
            //    		data:{
            //    			name:uname,
            //    			email:uemail,
            //    			phone:uphone,
            //    			city:ucity
            //    		},
            //    		success:function(response){
            //    			console.log(response);
            //    			if(response.status==200){
            //    				alert(response.msg);
            //    			}
            //    		}
            //    	});

            //    }else{
            //    	alert('Please Fill All The Fields !');
            //    }


            // });
		});
	</script>
</body>
</html>
