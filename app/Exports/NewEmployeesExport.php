<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\NewEmployee;
use Maatwebsite\Excel\Concerns\WithHeadings;

class NewEmployeesExport implements FromCollection,WithHeadings
{
    public function headings():array{
        return [
             "employee_id",
             'name',
             'dob',
             'gender',
             'address',
             'contact',
             'email',
             'pincode',
             'date_of_joining'
        ];
    }


    public function collection()
    {
         return collect(NewEmployee::all());
    }

}
