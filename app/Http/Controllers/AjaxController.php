<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Model\Ajaxs;
use DB;

class AjaxController extends Controller{

    public function insert_data(Request $request){

    	    $validator=\Validator::make(
            array('name'=>$request->name),array('name'=>'required|string|max:2056'),
            array('email'=>$request->email),array('email'=>'required|unique.ajaxs|email'),
            array('phone'=>$request->phone),array('phone'=>'requred|unique.ajaxs|digit'),
            array('city'=>$request->city),array('city'=>'nullable|string'),
            array('password'=>$request->password),array('password'=>'required|unique.ajaxs')
        );
        if($validator->fails()){
            return response()->json(['error_msg'=>'All field required !','status'=>201]); 
        }else{
     //dd($request->all());
           
         DB::table('ajaxs')
         ->insert([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'city'=>$request->city,
            'password'=>$request->password
        ]);
         return response()->json(['success_msg'=>'Data Inserted Successfully !','status'=>200]);
    }}

}
