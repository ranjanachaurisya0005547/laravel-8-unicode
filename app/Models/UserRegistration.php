<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
 
class UserRegistration extends Model {
	protected $table="useregistrations";
	protected $fillable=['name','email','password','token'];
	public $timestamps=false;

} 